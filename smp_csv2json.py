# JSON to python dict

import json
import datetime
import pandas as pd
import ntpath
import os
import PySimpleGUI as sg
import traceback

################################################################

# GUI settings

### theme
sg.theme('Reddit')

### layout
layout = [[sg.Text('Select Excel-File (.csv)')],
          [sg.Input(), sg.FileBrowse(key='-IN-'), sg.Submit(button_text="Convert")],
          # [sg.Input(), sg.FolderBrowse(key='-IN-'), sg.Submit(button_text="Convert")],
          [sg.Output(key='-OUTPUT-',size=(60,10))]]

### window
window = sg.Window('csv to JSON Converter', layout)

################################################################

# Event Loop


while True:
    event, values_smp = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'Convert':
        try:
            ### clear printed output
            window.FindElement('-OUTPUT-').Update('')

            ################################################################
            #####   Start basic app functionality ##########################
            ################################################################

            # Path Set up

            ### function to fix path OS vulnerability
            def path_tail(path):
                head, tail = ntpath.split(path)
                return tail or ntpath.basename(head)

            def path_head(path):
                head, tail = ntpath.split(path)
                return head or ntpath.basename(tail)

            ### full path (dir + file + ext)
            fullpath = values_smp['-IN-']
            # fullpath = files_smp['-IN-']

            ### extract dir from fullpath
            dironly = path_head(fullpath)

            ### extract filename from fullpath
            filename_ext = path_tail(fullpath)
            filename = os.path.splitext(filename_ext)[0]

            ### define output path + file name
            d_out = os.path.join(dironly, filename + ".json")

            ### extract output file name
            filename_new = path_tail(d_out)

            ################################################################

            # load the input excel from disk


            print("")
            print(f"[INFO] Loading input file {filename_ext}...")
            print("")
            df = pd.read_csv(values_smp["-IN-"], header=0)#, sheet_name="Profile", header=9)

            print("[INFO] Serializing file...")
            print("")

            ### make all values_smp integer (needed to convert to json later)
            df = df.astype("object")

            ### remve linebreaks and spaces from df header
            df.columns = df.columns.map(lambda x: x.replace('\n','_').replace(' ', '_'))

            ### Converter
            def myconverter(o):
                if isinstance(o, datetime.datetime):
                    return o.__str__()

            ################################################################

            smp_df = df[["distance_[mm]", "force_median_[N]"]].dropna()

            ### reset index in case there are spaces (i.e. empty values_smp) in excel / df
            smp_df = smp_df.reset_index(drop=True)
            smp_df.iloc[:,0] = smp_df.iloc[:,0].div(10)

            ### change name from xxx_eth to xxx (to match the version of the final json)
            smp_df.rename(columns={"distance_[mm]": "depth", "force_median_[N]": "value"}, inplace=True)

            values_smp = {}
            values_smp_dict= {}
            layers_smp = []

            for i, row in smp_df.iterrows():
                values_smp[i] = dict(smp_df.iloc[i, 0:2])
                values_smp_dict[i] = {"value": values_smp[i]}
                ### put results into list ---> "layers": [ {"value":{"depth": ..., "value": ...}}, { .... }, { .... } ]
                layers_smp.append(values_smp_dict[i])


            # Create profile structure ("meta") --> ETH Sampler (level 4)
            smp_meta_dict = {"pointprofile": False, "method": "SnowMicroPen", "comment": ""}

            ################################################################

            # Create measurement dictionaries (level 2)

            ## Density (layers_cut, layers_eth, ...) (level 3)

            ###  "density" : { elements: [ { "layers": LAYERS_ETH }, {"layers": LAYERS_CUT }, {"layers": DEN_RHO} ] }
            ### add META here
            layers2_smp = {}

            ### only append layers (and meta) if there is an ETH/CUT/Denoth profile (...values_smp_dict dict has positive length)
            if len(values_smp_dict) > 0:
                layers2_smp["layers"] = layers_smp
                layers2_smp["meta"] = smp_meta_dict

            ### store dicts inside list, if they contain something
            elements_smp_list = []

            if len(layers2_smp) > 0:
                elements_smp_list.append(layers2_smp)

            ### add "type" --> "hardness" : { elements: [ ... ] , "type" : "hardness" }
            smp_dict = {"elements": elements_smp_list, "type": "smp"}
            
            profiles_dict = {}
            if len(elements_smp_list) > 0:
                profiles_dict["smp"] = smp_dict

            profiles_list = [profiles_dict]

            final_dict = {"profiles": profiles_dict}

            ### info: default=myconverter is used to serialize datetime object (function "myconverter" defined above)
            with open(d_out, "w") as json_file:
                json.dump(profiles_dict, json_file, default = myconverter)

            # done, give feedack to user

            print("[INFO] Done! :-)")
            print("")
            print(f'[INFO] Wrote new file: "{d_out}"')
            print("")
            print(f'[INFO] To append to existing Profile, run add2profile.py')
            print("")

            # Error handling

        except Exception as e:
            tb = traceback.format_exc()
            sg.Print(f'Ups, an error occured =( \n\n Here is the info:', e, tb)
            window.FindElement('-OUTPUT-').Update('')
# JSON to python dict

import json
import pprint
import datetime
import pandas as pd

import argparse
import ntpath
import os

import PySimpleGUI as sg
import traceback

################################################################

# GUI settings

### theme
sg.theme('Reddit')

### layout
layout = [[sg.Text('Select Excel-File (.xlsx)')],
          [sg.Input(), sg.FileBrowse(key='-IN-'), sg.Submit(button_text="Convert")],
          [sg.Output(key='-OUTPUT-',size=(60,10))]]

### window
window = sg.Window('Excel to JSON Converter', layout)

################################################################

# Event Loop


while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'Convert':
        try:
            ### clear printed output
            window.FindElement('-OUTPUT-').Update('')

            ################################################################
            #####   Start basic app functionality ##########################
            ################################################################

            # Path Set up

            ### function to fix path OS vulnerability
            def path_tail(path):
                head, tail = ntpath.split(path)
                return tail or ntpath.basename(head)

            def path_head(path):
                head, tail = ntpath.split(path)
                return head or ntpath.basename(tail)

            ### full path (dir + file + ext)
            fullpath = values['-IN-']

            ### extract dir from fullpath
            dironly = path_head(fullpath)

            ### extract filename from fullpath
            filename_ext = path_tail(fullpath)
            filename = os.path.splitext(filename_ext)[0]

            ### define output path + file name
            d_out = os.path.join(dironly, filename + ".json")

            ### extract output file name
            filename_new = path_tail(d_out)

            ################################################################

            # load the input excel from disk


            print("")
            print(f"[INFO] Loading input file {filename_ext}...")
            print("")
            df = pd.read_excel(values["-IN-"], sheet_name="Profile", header=9)

            print("[INFO] Serializing file...")
            print("")

            ### make all values integer (needed to convert to json later)
            df = df.astype("object")

            ### remve linebreaks and spaces from df header
            df.columns = df.columns.map(lambda x: x.replace('\n','_').replace(' ', '_'))

smp_df = df[["distance [mm]", "force_median [N]"]].dropna()

### reset index in case there are spaces (i.e. empty values) in excel / df
smp_df = smp_df.reset_index(drop=True)

### change name from xxx_eth to xxx (to match the version of the final json)
smp_df.rename(columns={"depth [mm]": "top", "median force [N]": "value"}, inplace=True)

values = {}

for i, row in smp_df.iterrows():
    values[i] = dict(smp_df.iloc[i, 0:3])

### put results into list ---> "layers": [ {"top": ..., "bottom": ..., "value": ...}, { .... }, { .... } ]
layers_smp = []

for k, v in values.items():
    layers_eth.append(values[k])

# Create profile structure ("meta") --> ETH Sampler (level 4)
eth_meta_dict = {"pointprofile": False, "method": "Snow Cylinder", "comment": ""}

################################################################

# Create measurement dictionaries (level 2)

## Density (layers_cut, layers_eth, ...) (level 3)

###  "density" : { elements: [ { "layers": LAYERS_ETH }, {"layers": LAYERS_CUT }, {"layers": DEN_RHO} ] }
### add META here
layers2_eth = {}

### only append layers (and meta) if there is an ETH/CUT/Denoth profile (...values dict has positive length)
if len(values) > 0:
    layers2_eth["layers"] = layers_eth
    layers2_eth["meta"] = eth_meta_dict

### store dicts inside list, if they contain something
elements_list = []

if len(layers2_eth) > 0:
    elements_list.append(layers2_eth)

### add "type" --> "hardness" : { elements: [ ... ] , "type" : "hardness" }
hardness_dict = {"elements": elements_list, "type": "hardness"}
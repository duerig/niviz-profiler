import json
import datetime
import ntpath
import os
import PySimpleGUI as sg
import traceback



# GUI settings

### theme
sg.theme('Reddit')

### layout
layout = [[sg.Text('Select JSON-File (.json)')],
          [sg.Input(), sg.FileBrowse(key='-IN1-')],
          [sg.Text('Select smp-File to append (.json)')],
          [sg.Input(), sg.FileBrowse(key='-IN2-'), sg.Submit(button_text="Append")],
          [sg.Output(key='-OUTPUT-',size=(60,10))]]

### window
window = sg.Window('JSON appender', layout)

# Path Set up

### function to fix path OS vulnerability
def path_tail(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def path_head(path):
    head, tail = ntpath.split(path)
    return head or ntpath.basename(tail)

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'Append':
        try:
            ### clear printed output
            window.FindElement('-OUTPUT-').Update('')
            ### full path (dir + file + ext)
            fullpath1 = values['-IN1-']
            ### extract dir from fullpath
            dironly1 = path_head(fullpath1)
            ### extract filename from fullpath
            filename_ext1 = path_tail(fullpath1)
            filename1 = os.path.splitext(filename_ext1)[0]
            ### define output path + file name
            d_out = os.path.join(dironly1, filename1 + "_extended.json")
            ### extract output file name
            filename_new1 = path_tail(d_out)

            ### full path (dir + file + ext)
            fullpath2 = values['-IN2-']
            ### extract dir from fullpath
            dironly2 = path_head(fullpath2)
            ### extract filename from fullpath
            filename_ext2 = path_tail(fullpath2)
            filename2 = os.path.splitext(filename_ext2)[0]
            ### extract output file name

            # function to add to JSON

            def myconverter(o):
                if isinstance(o, datetime.datetime):
                    return o.__str__()

            def write_json(new_data, filename, d_out):
                with open(filename,'r+') as file1, open(new_data, 'r') as file2:
                    # First we load existing data into a dict.
                    file_data = json.load(file1)
                    file_append = json.load(file2)
                    profiles_dict = {}

                    # Typical structure of a exported SimpleProfile.json from NiViz
                    if type(file_data) is dict:
                        for i in file_data.keys():
                            if type(file_data[i]) is list:
                                for j in range(len(file_data[i])):
                                    if type(file_data[i][j]) is dict:
                                        for k in file_data[i][j].keys():
                                            profiles_dict[k] = file_data[i][j][k]

                    if type(file_append) is dict:
                        for l in file_append.keys():
                            if l in profiles_dict:
                                profiles_dict[l]["elements"].append(file_append[l]["elements"][0])
                            else:
                                profiles_dict[l] = file_append[l]
                    else:
                        print("Format of appended file not correct")

                    ### Remove empty values (only applicable for temp atm --> density is already done above because it is nested and there is always at least one density value)
                    profiles_list = [profiles_dict]
                    final_dict = {"profiles": profiles_list, "name": file_data["name"], "id": file_data["id"],
                                  "position": file_data["position"]}

                    with open(d_out, 'w') as outfile:
                        json.dump(final_dict, outfile, indent=2, default=myconverter)

            write_json(fullpath2, fullpath1, d_out)

        except Exception as e:
            tb = traceback.format_exc()
            sg.Print(f'Ups, an error occured =( \n\n Here is the info:', e, tb)
            window.FindElement('-OUTPUT-').Update('')
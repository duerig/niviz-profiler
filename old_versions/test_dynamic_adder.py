import json
import datetime
import ntpath
import os
import PySimpleGUI as sg
import traceback

#### GUI settings
### theme
sg.theme('Reddit')

### layout
layout = [[sg.Text('Select JSON-File (.json)')],
          [sg.Input(), sg.FileBrowse(key='-IN1-')],
          [sg.Text('Select smp-File to append (.json)')],
          [sg.Input(), sg.FileBrowse(key='-IN2-'), sg.Submit(button_text="Append")],
          [sg.Output(key='-OUTPUT-',size=(60,10))]]

### window
window = sg.Window('JSON appender', layout)

#### Path Set up

### function to fix path OS vulnerability
def path_tail(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

def path_head(path):
    head, tail = ntpath.split(path)
    return head or ntpath.basename(tail)

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'Append':
        try:
            ### clear printed output
            window.FindElement('-OUTPUT-').Update('')
            ### file1
            ### full path (dir + file + ext)
            fullpath1 = values['-IN1-']
            ### extract dir from fullpath
            dironly1 = path_head(fullpath1)
            ### extract filename from fullpath
            filename_ext1 = path_tail(fullpath1)
            filename1 = os.path.splitext(filename_ext1)[0]
            ### define output path + file name
            d_out = os.path.join(dironly1, filename1 + "_extended.json")
            ### extract output file name
            filename_new1 = path_tail(d_out)

            ### file2
            ### full path (dir + file + ext)
            fullpath2 = values['-IN2-']
            ### extract dir from fullpath
            dironly2 = path_head(fullpath2)
            ### extract filename from fullpath
            filename_ext2 = path_tail(fullpath2)
            filename2 = os.path.splitext(filename_ext2)[0]
            ### extract output file name

            ### handle datetime object
            def myconverter(o):
                if isinstance(o, datetime.datetime):
                    return o.__str__()

            ### function to add to JSON
            def write_json(new_data, filename, d_out):
                with open(filename,'r+') as file1, open(new_data, 'r') as file2:
                    # possible features found in niviz-master\lib\features.js obtained from https://models.slf.ch/p/niviz/source/tree/master/
                    features = ['grainshape', 'grainsize', 'density', 'gradient', 'hardness', 'ramm',
                                'wetness', 'ssa', 'sk38', 'ct', 'ect', 'rb', 'sf', 'saw', 'threads', 'temperature',
                                'smp', 'impurity', 'thickness', 'dendricity', 'comments', 'sphericity', 'bondsize', 'flags',
                                'cn', 'critical', 'hardnessBottom']

                    # file_data_test = str(file_data).replace('[', '').replace(']', '').replace("'",'"')
                    # # file_data_test = str(file_data).replace('[', '').replace(']', '')
                    # # print(file_data_test)
                    # outfile = open ("test.json", "w")
                    # file_data_test = json.dumps(file_data_test)#, outfile)
                    # file_data_test = json.loads(file_data_test)
                    # with open('data{}.json', 'a') as f:
                    #     f.write(file_data_test + '\n')
                    # with open(ntpath.normpath(r"H:\Zivi\LAR\data{}.json"), 'r+') as file3:
                    #     json.load(file3)

                    # json.load(file)
                    # json.dump(file_data_test,outfile)
                    # print(file_data_test)
                    # asdfaf = json.loads(file_data_test)
                    # df = pd.json_normalize(file_data)
                    # df = pd.DataFrame(df)
                    # print(df)

                    # for key in features:
                    #     # print(file_data.keys())
                    #     if key in file_data["profiles"][0].keys():
                    #         pass
                    #         # print(key)

                    # print(file_data["profiles"][0].keys())


                    ### load JSON files
                    file_data = json.load(file1)
                    file_append = json.load(file2)

                    # returns depth of structure
                    def dict_depth(dic, level=0):
                        if not isinstance(dic, (dict, list)) or not dic:
                            return level
                        return max(dict_depth(dic[key], level + 1) for key in dic) if isinstance(dic, dict) else max(
                            dict_depth(dic[key], level + 1) for key in range(len(dic)))

                    # returns depth of features (necessary for correct appending)
                    def feature_depth(dic, level=1):
                        for i in range(dict_depth(dic)):
                            if isinstance(dic, dict) and not any(key in features for key in dic.keys()):
                                for key in dic.keys():
                                    dic = dic[key]
                                    level += 1
                                    break
                            elif isinstance(dic, list):
                                for key in range(len(dic)):
                                    dic = dic[key]
                                    # level += 1
                                    break
                            else:
                                return level
                        return level

                    file_data_depth = feature_depth(file_data)
                    file_append_depth = feature_depth(file_append)

                    if file_append_depth > file_data_depth: # define deeper profile as "base"
                        file_data_tmp = file_data
                        file_data = file_append
                        file_append = file_data_tmp
                        file_data_depth = feature_depth(file_data)
                        file_append_depth = feature_depth(file_append)

                    profiles_dict = {}
                    # Typical structure of a exported SimpleProfile.json from NiViz
                    def file_appender(file_data, file_append, file_data_depth, file_append_depth):
                        if file_data_depth == 1: # the case if only one element and not a profile
                            if type(file_data) is dict:
                                for i in file_data.keys():
                                    # create profiles_dict with keys from source data file
                                    profiles_dict[i] = file_data[i]
                        elif file_data_depth == 2:   # usually the case if a profile (either exported from NiViz or created with "Excel_Converter"
                            if type(file_data) is dict: # usually first layer of a profile is "profiles"
                                for i in file_data.keys():
                                    if type(file_data[i]) is list:  # usually "elements" & "layers" contain lists
                                        for j in range(len(file_data[i])):
                                            if type(file_data[i][j]) is dict:   # Within the list are the features
                                                for k in file_data[i][j].keys():
                                                    profiles_dict[k] = file_data[i][j][k]
                        else:
                            print("yet unknown data strcuture")

                        if file_append_depth == 1:   # usually the case if file_append contains only an "element" like SMP measurements
                            if type(file_append) is dict:
                                for l in file_append.keys():    # l = key of dict
                                    if l in profiles_dict and l in features:  # If "element" (key) already exists in file_data, append "layers" in "elements"
                                        for m in range(len(file_append[l]["elements"])):
                                            print(m)
                                            profiles_dict[l]["elements"].append(file_append[l]["elements"][m])
                                    else:   # If "element" (key) is new, create new element in profiles_dict
                                        profiles_dict[l] = file_append[l]
                        elif file_append_depth == 2:
                            if type(file_append) is dict:
                                for l in file_append.keys():
                                    print(l)
                                    if l == "profiles" and type(file_append[l]) is list:
                                        for m in range(len(file_append[l])):
                                            if type(file_append[l][m]) is dict:
                                                for n in file_append[l][m]:
                                                    if n in profiles_dict and n in features:
                                                        for o in range(len(file_append[l][m][n]["elements"])):
                                                            profiles_dict[n]["elements"].append(file_append[l][m][n]["elements"][o])
                                                    else:
                                                        print(n)
                                                        profiles_dict[l] = file_append[l][m][n]
                        else:
                            print("Format of appended file not correct")

                    file_appender(file_data, file_append, file_data_depth, file_append_depth)

                    ### Remove empty values (only applicable for temp atm --> density is already done above because it is nested and there is always at least one density value)
                    profiles_list = [profiles_dict]

                    final_dict = {"profiles": profiles_list, "name": file_data["name"], "id": file_data["id"],
                                "position": file_data["position"]}

                    with open(d_out, 'w') as outfile:
                        json.dump(final_dict, outfile, indent=2, default=myconverter)
                        print("saved to: " + d_out)

            # Call function to write new JSON
            write_json(fullpath2, fullpath1, d_out)

        except Exception as e:
            tb = traceback.format_exc()
            sg.Print(f'Ups, an error occured =( \n\n Here is the info:', e, tb)
            window.FindElement('-OUTPUT-').Update('')
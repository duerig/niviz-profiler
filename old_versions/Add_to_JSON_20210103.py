import json
import datetime
import ntpath
import os
import PySimpleGUI as sg
import traceback



# GUI settings

### theme
sg.theme('Reddit')

### layout
layout = [[sg.Text('Select JSON-File (.json)')],
          [sg.Input(), sg.FileBrowse(key='-IN1-')],
          [sg.Text('Select smp-File to append (.json)')],
          [sg.Input(), sg.FileBrowse(key='-IN2-'), sg.Submit(button_text="Append")],
          [sg.Output(key='-OUTPUT-',size=(60,10))]]

### window
window = sg.Window('JSON appender', layout)

# Path Set up

### function to fix path OS vulnerability
def path_tail(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def path_head(path):
    head, tail = ntpath.split(path)
    return head or ntpath.basename(tail)

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'Append':
        try:
            ### clear printed output
            window.FindElement('-OUTPUT-').Update('')
            ### full path (dir + file + ext)
            fullpath1 = values['-IN1-']
            ### extract dir from fullpath
            dironly1 = path_head(fullpath1)
            ### extract filename from fullpath
            filename_ext1 = path_tail(fullpath1)
            filename1 = os.path.splitext(filename_ext1)[0]
            ### define output path + file name
            d_out = os.path.join(dironly1, filename1 + "_extended.json")
            ### extract output file name
            filename_new1 = path_tail(d_out)

            ### full path (dir + file + ext)
            fullpath2 = values['-IN2-']
            ### extract dir from fullpath
            dironly2 = path_head(fullpath2)
            ### extract filename from fullpath
            filename_ext2 = path_tail(fullpath2)
            filename2 = os.path.splitext(filename_ext2)[0]
            ### extract output file name

                # function to add to JSON

            def myconverter(o):
                if isinstance(o, datetime.datetime):
                    return o.__str__()

            def write_json(new_data, filename, d_out):
                with open(filename,'r+') as file1, open(new_data, 'r') as file2:
                    # First we load existing data into a dict.
                    file_data = json.load(file1)
                    file_data_list = file_data["profiles"]
                    file_data_dict = file_data_list[0]

                    file_append = json.load(file2)
                    #file_append_list = file_append["profiles"]
                    #file_append_dict = file_append_list[0]
                    profiles_dict = {}
                    for i in file_data_dict.keys():
                        profiles_dict[i] = file_data_dict[i]

                    for j in file_append.keys():
                        if j in profiles_dict:
                            profiles_dict[j]["elements"].append(file_append[j]["elements"][0])
                        else:
                            profiles_dict[j] = file_append[j]


                    '''
                    if "density" in file_data_dict:
                        density_dict = file_data_dict["density"]
                        profiles_dict["density"] = density_dict
                    if "wetness" in file_data_dict:
                        lwc_dict = file_data_dict["wetness"]
                        profiles_dict["wetness"] = lwc_dict
                    if "ssa" in file_data_dict:
                        ssa_dict = file_data_dict["ssa"]
                        profiles_dict["ssa"] = ssa_dict
                    if "temperature" in file_data_dict:
                        tp_dict = file_data_dict["temperature"]
                        profiles_dict["temperature"] = tp_dict
                    if "smp" in file_data_dict:
                        smp_dict = file_data_dict["smp"]
                        profiles_dict["smp"] = smp_dict
                    if "hardness" in file_data_dict:
                        hardness_dict = file_data_dict["hardness"]
                        profiles_dict["hardness"] = hardness_dict
                    if "grainshape" in file_data_dict:
                        grainshape_dict = file_data_dict["grainshape"]
                        profiles_dict["grainshape"] = grainshape_dict
                    if "grainsize" in file_data_dict:
                        grainsize_dict = file_data_dict["grainsize"]
                        profiles_dict["grainsize"] = grainsize_dict
                    if "comments" in file_data_dict:
                        comments_dict = file_data_dict["comments"]
                        profiles_dict["comments"] = comments_dict
                    if "info" in file_data_dict:
                        info_dict = file_data_dict["info"]
                        profiles_dict["info"] = info_dict
                    if "date" in file_data_dict:
                        date_dict = file_data_dict["date"]
                        profiles_dict["date"] = date_dict
                
                    ## Check for dicts to append
                    if "density" in file_append:
                        density_dict_append = file_append["density"]
                        if "density" in file_data_dict:
                            profiles_dict["density"]["elements"].append(density_dict_append["elements"][0])
                        else:
                            profiles_dict["density"] = density_dict_append
                    if "wetness" in file_append:
                        lwc_dict_append = file_append["wetness"]
                        if "lwc" in file_data_dict:
                            profiles_dict["lwc"]["elements"].append(lwc_dict_append["elements"][0])
                        else:
                            profiles_dict["lwc"] = lwc_dict_append
                    if "ssa" in file_append:
                        ssa_dict_append = file_append["ssa"]
                        if "ssa" in file_data_dict:
                            profiles_dict["ssa"]["elements"].append(ssa_dict_append["elements"][0])
                        else:
                            profiles_dict["ssa"] = ssa_dict_append
                    if "temperature" in file_append:
                        tp_dict_append = file_append["temperature"]
                        if "tp" in file_data_dict:
                            profiles_dict["tp"]["elements"].append(tp_dict_append["elements"][0])
                        else:
                            profiles_dict["tp"] = tp_dict_append
                    if "smp" in file_append:
                        smp_dict_append = file_append["smp"]
                        if "smp" in file_data_dict:
                            profiles_dict["smp"]["elements"].append(smp_dict_append["elements"][0])
                        else:
                            profiles_dict["smp"] = smp_dict_append
                    if "hardness" in file_append:
                        hardness_dict_append = file_append["hardness"]
                        if "hardness" in file_data_dict:
                            profiles_dict["hardness"]["elements"].append(hardness_dict_append["elements"][0])
                        else:
                            profiles_dict["hardness"] = hardness_dict_append
                    if "grainshape" in file_append:
                        grainshape_dict_append = file_append["grainshape"]
                        if "grainshape" in file_data_dict:
                            profiles_dict["grainshape"]["elements"].append(grainshape_dict_append["elements"][0])
                        else:
                            profiles_dict["grainshape"] = grainshape_dict_append
                    if "grainsize" in file_append:
                        grainsize_dict_append = file_append["grainsize"]
                        if "grainsize" in file_data_dict:
                            profiles_dict["grainsize"]["elements"].append(grainsize_dict_append["elements"][0])
                        else:
                            profiles_dict["grainsize"] = grainsize_dict_append
                            '''
                    
                    ### Remove empty values (only applicable for temp atm --> density is already done above because it is nested and there is always at least one density value)
                    profiles_list = [profiles_dict]

                    final_dict = {"profiles": profiles_list, "name": file_data["name"], "id": file_data["id"],
                                  "position": file_data["position"]}

                    # Join new_data with file_data_dict inside emp_details
                    # file_data_dict["profiles"].insert(0,file_append)
                    # file_data_dict["smp"] = file_append
                    # Sets file's current position at offset.
                    # file1.seek(0)
                    # convert back to json.
                    with open(d_out, 'w') as outfile:
                        json.dump(final_dict, outfile, indent=2, default=myconverter)

                    #json.dump(file_data_dict, file, indent = 2)

            write_json(fullpath2, fullpath1, d_out)

        except Exception as e:
            tb = traceback.format_exc()
            sg.Print(f'Ups, an error occured =( \n\n Here is the info:', e, tb)
            window.FindElement('-OUTPUT-').Update('')